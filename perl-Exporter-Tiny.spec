Name:           perl-Exporter-Tiny
Version:        1.002001
Release:        5
Summary:        An exporter with the features of Sub::Exporter but only core dependencies
License:        GPL+ or Artistic
URL:            https://metacpan.org/release/Exporter-Tiny
Source0:        https://cpan.metacpan.org/modules/by-module/Exporter/Exporter-Tiny-1.002001.tar.gz
BuildArch:      noarch
BuildRequires:  coreutils findutils make
BuildRequires:  perl-generators perl-interpreter
BuildRequires:  perl(ExtUtils::MakeMaker) >= 6.17
BuildRequires:  perl(Carp) perl(strict) perl(warnings)
BuildRequires:  perl(lib) perl(Test::More) >= 0.47
Requires:       perl(:MODULE_COMPAT_%(eval "`perl -V:version`"; echo $version)) perl(Carp)

%{?perl_default_filter}

%description
Exporter::Tiny supports many of Sub::Exporter's external-facing features
including renaming imported functions with the -as, -prefix and -suffix
options; explicit destinations with the into option; and alternative
installers with the installer option. But it's written in only about 40%
as many lines of code and with zero non-core dependencies.
Its internal-facing interface is closer to Exporter.pm, with configuration
done through the @EXPORT, @EXPORT_OK and %%EXPORT_TAGS package variables.

%package_help

%prep
%autosetup -n Exporter-Tiny-1.002001 -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PERLLOCAL=1 NO_PACKLIST=1
%make_build

%install
%make_install
%{_fixperms} -c %{buildroot}

%check
make test

%files
%doc CREDITS examples/ README
%license COPYRIGHT LICENSE
%{perl_vendorlib}/Exporter/

%files help
%doc Changes TODO
%{_mandir}/man3/*

%changelog
* Sat Oct 26 2019 shenyangyang <shenyangyang4@huawei.com> - 1.002001-5
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:delete unneeded build requires of perl(Test::Warnings)

* Tue Oct 15 2019 shenyangyang <shenyangyang4@huawei.com> - 1.002001-4
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:delete unneeded build requires

* Sun Sep 29 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.002001-3
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: revise requires and change the directory of some files 

* Sat Sep 14 2019 zhangsaisai <zhangsaisai@huawei.com> - 1.002001-2
- Package init
